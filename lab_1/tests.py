from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index_lab1, mhs_name, calculate_age
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_hello_name_is_exist(self):
        response = Client().get('/lab-1/')
        self.assertEqual(response.status_code,404)

    # def test_using_index_func(self):
    #     found = resolve('/lab-1/')
    #     self.assertEqual(found.func, index_lab1)

    def test_name_is_changed(self):
        request = HttpRequest()
        response = index_lab1(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>' + mhs_name + '</title>', html_response)
        self.assertIn('<h1>Hello my name is ' + mhs_name + '</h1>', html_response)
        self.assertFalse(len(mhs_name) == 0)


    def test_calculate_age_is_correct(self):
        self.assertEqual(0, calculate_age(date.today().year))
        self.assertEqual(18, calculate_age(2000))
        self.assertEqual(28, calculate_age(1990))


    def test_index_contains_age(self):
        request = HttpRequest()
        response = index_lab1(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<article>I am [0-9]\d+ years old</article>')
