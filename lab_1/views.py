from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Afiful Amin' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 12, 29) #TODO Implement this, format (Year, Month, Date)
npm = 1706039963 # TODO Implement this
# Create your views here.
def index_lab1(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
