from .views import index, add_jadwal, daftar_jadwal
from django.urls import path, include

urlpatterns = [
    path('', index, name='index'),
    path('daftar/', daftar_jadwal, name='daftar'),
    path('add_jadwal', add_jadwal, name='add_jadwal'),
    path('daftar_jadwal', daftar_jadwal, name='daftar_jadwal'),
]