from django.db import models

class Jadwal(models.Model):
    kegiatan = models.CharField(max_length=27)
    tempat = models.CharField(max_length=40)
    kategori = models.CharField(max_length=12)
    waktu = models.DateTimeField(auto_now_add=True,blank=True)

    def __str__(self):
        return self.kegiatan
