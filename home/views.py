from django.shortcuts import render
from .forms import Jadwal_Form
from .models import Jadwal
from django.http import HttpResponseRedirect

name = 'M AFIFUL AMIN'
html = 'index.html'
# Create your views here.
def index(request):
    response = {'name' : name}
    response['jadwal_form'] = Jadwal_Form
    return render(request, html, response)

def add_jadwal(request):
    form = Jadwal_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['kegiatan'] = request.POST['kegiatan'] if request.POST['kegiatan'] != "" else "Anonymous"
        response['tempat'] = request.POST['tempat'] if request.POST['tempat'] != "" else "-"
        response['kategori'] = request.POST['kategori'] if request.POST['ketegori'] != "" else "Anonymous"
        response['waktu'] = request.POST['waktu'] if request.POST['waktu'] != "" else "Anonymous"
        
        jadwal = Jadwal(kegiatan=response['kegiatan'], tempat=response['tempat'],
                kategori=response['kategori'], waktu=response['waktu'])
        jadwal.save()
        # html ='home/daftar.html'
        return render(request, 'home/daftar.html', response)
    else:        
        return HttpResponseRedirect('/home/daftar.html')

def daftar_jadwal(request):
        jadwal = Jadwal.objects.all()
        response['jadwal'] = jadwal
        html = 'home/daftar.html'
        return render(request, html , response)
