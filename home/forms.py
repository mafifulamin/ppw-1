from django import forms
from datetime import datetime

class Jadwal_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control'
    }

    kegiatan = forms.CharField(label='Nama Kegiatan', required=True,
        max_length=27, widget=forms.TextInput(attrs=attrs))
    tempat = forms.EmailField(label='Tempat', required=False,
        max_length=40, widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='Kategori', required=True,
        max_length=12, widget=forms.TextInput(attrs=attrs))
    waktu = forms.DateTimeField(label='Waktu', required=True,
        initial=datetime.now())